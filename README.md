- First, let’s clone the repository with our code:  
git clone https://wieireneusz@bitbucket.org/wieireneusz/ansta_django.git  

- Start a virtual environment:  
virtualenv venv -p python3  

- Initialize the virtualenv:  
source venv/bin/activate  

- Install the requirements:  
pip install -r ansta_django/requirements.txt  

- Now let’s migrate the database, create SECRET_KEY and create a super user:  
cd ansta_django/contacts  
create secrets.py in contacts(next to settings.py) with SECRET_KEY  
python manage.py migrate  
python manage.py createsuperuser  

- Load fixtures  
python manage.py loaddata contact/fixtures/contacts.json  

- Start project/server  
python manage.py runserver  