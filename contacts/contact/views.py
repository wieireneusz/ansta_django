from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import (
    CreateView,
    DeleteView,
    ListView,
    UpdateView
)
from .models import Person, Phone, Email
from .forms import (
    PersonModelForm,
    PhoneModelForm,
    EmailModelForm
)


class PersonListView(ListView):
    template_name = 'list.html'
    queryset = Person.objects.all()

    def get_queryset(self):
        query = self.request.GET.get('q', None)
        queryset = self.queryset
        if query is not None:
            queryset = Person.objects.search(query=query)
            for person in queryset:
                person.phone = Phone.objects.filter(person=person)
                person.email = Email.objects.filter(person=person)
        else:
            queryset = Person.objects.all()
            for person in queryset:
                person.phone = Phone.objects.filter(person=person)
                person.email = Email.objects.filter(person=person)
        return queryset

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['query'] = self.request.GET.get('q')
        return context


class PersonAddView(CreateView):
    template_name = 'contact/person_add.html'
    form_class = PersonModelForm

    def get_success_url(self):
        return reverse_lazy('contact:list')


class UpdatePersonView(UpdateView):
    template_name = 'contact/person_update.html'
    form_class = PersonModelForm
    queryset = Person.objects.all()

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Person, id=id_)

    def get_success_url(self):
        return reverse_lazy('contact:list')


class PersonDeleteView(DeleteView):
    template_name = 'contact/person_delete.html'
    fail_url = 'contact/person_delete_fail.html'

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Person, id=id_)

    def get_success_url(self):
        return reverse_lazy('contact:list')

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        fail_url = self.fail_url
        if Phone.objects.filter(person=self.object) or Email.objects.filter(person=self.object):
            return render(request, fail_url)
        else:
            self.object.delete()
            return redirect(success_url)


class PhoneAddView(CreateView):
    template_name = 'contact/phone_add.html'
    form_class = PhoneModelForm

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.person = Person.objects.get(id=self.kwargs.get("id"))
        self.object = obj.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('contact:list')


class PhoneDeleteView(DeleteView):
    template_name = 'contact/phone_delete.html'

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Phone, id=id_)

    def get_success_url(self):
        return reverse_lazy('contact:list')


class EmailAddView(CreateView):
    template_name = 'contact/email_add.html'
    form_class = EmailModelForm

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.person = Person.objects.get(id=self.kwargs.get("id"))
        self.object = obj.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('contact:list')


class EmailDeleteView(DeleteView):
    template_name = 'contact/email_delete.html'

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Email, id=id_)

    def get_success_url(self):
        return reverse_lazy('contact:list')
