from django.urls import path
from contact.views import (
    PersonListView,
    PersonAddView,
    UpdatePersonView,
    PersonDeleteView,
    PhoneAddView,
    EmailAddView,
    PhoneDeleteView,
    EmailDeleteView,
)

app_name = 'contact'
urlpatterns = [
    path('list', PersonListView.as_view(), name='list'),
    path('add', PersonAddView.as_view(), name='person-add'),
    path('<int:id>/update', UpdatePersonView.as_view(), name='person-update'),
    path('<int:id>/delete', PersonDeleteView.as_view(), name='person-delete'),
    path('<int:id>/phone/add', PhoneAddView.as_view(), name='phone-add'),
    path('phone/<int:id>/delete', PhoneDeleteView.as_view(), name='phone-delete'),
    path('<int:id>/email/add', EmailAddView.as_view(), name='email-add'),
    path('email/<int:id>/delete', EmailDeleteView.as_view(), name='email-delete'),
]
