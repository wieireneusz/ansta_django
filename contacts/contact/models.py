from django.db import models
from django.db.models import Q
from django.core.validators import RegexValidator
from django.urls import reverse


class ContactQuerySet(models.QuerySet):
    def search(self, query=None):
        lookup = (
            Q(name__icontains=query) |
            Q(surname__icontains=query) |
            Q(email__email__icontains=query) |
            Q(phone__phone__icontains=query)
        )
        return self.filter(lookup)


class ContactManager(models.Manager):
    def get_queryset(self):
        return ContactQuerySet(self.model, using=self._db)

    def search(self, query=None):
        if query is None:
            return self.get_queryset().none()
        return self.get_queryset().search(query=query)


class Person(models.Model):
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)

    objects = ContactManager()

    def update_url(self):
        return reverse('contact:person-update', kwargs={'id': self.id})

    def delete_url(self):
        return reverse('contact:person-delete', kwargs={'id': self.id})

    def phone_add_url(self):
        return reverse('contact:phone-add', kwargs={'id': self.id})

    def email_add_url(self):
        return reverse('contact:email-add', kwargs={'id': self.id})


class Phone(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    phone_regex = RegexValidator(
        regex=r'^\+?1?\d{9,15}$',
        message="Phone number must be entered in the format: '+999999999'. \
            Up to 19 digits allowed."
        )
    phone = models.CharField(validators=[phone_regex], max_length=20)

    def delete_url(self):
        return reverse('contact:phone-delete', kwargs={'id': self.id})


class Email(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    email = models.EmailField()

    def delete_url(self):
        return reverse('contact:email-delete', kwargs={'id': self.id})
