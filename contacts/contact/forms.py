from django import forms
from .models import Person, Phone, Email


class PersonModelForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = [
            'name',
            'surname'
        ]


class PhoneModelForm(forms.ModelForm):
    class Meta:
        model = Phone
        fields = [
            'phone'
        ]


class EmailModelForm(forms.ModelForm):
    class Meta:
        model = Email
        fields = [
            'email'
        ]
